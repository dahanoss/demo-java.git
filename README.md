# 大汉三通 OSS API JAVA DEMO

提供一系列大汉三通接口通讯云OSS调用的SDK

#### 目录结构
```
└─demo-java
    └─error-phone-demo
        ├─ErrorPhoneDemo
    └─mms-demo
        ├─Constant
        ├─MmsClientDemo
        ├─MmsReceiveMain
        ├─MmsReportReceiveServlet
    └─shorturl-demo
        ├─Constant
        ├─ShortUrlDemo
        ├─ShortUrlDetailReceiveMain
        ├─ShortUrlDetailReceiveServlet
    └─sms-demo
        ├─Constant
        ├─SmsClientDemo
        ├─SmsDeliverReceiveServlet
        ├─SmsReceiveMain
        ├─SmsReportReceiveServlet
        ├─SmsTemplateClientDemo
    └─smsstatistics-demo
        ├─Constant
        ├─SmsStatisticsClientDemo
    └─supermsg-demo
        ├─ExamineReportServer
        ├─SupermsgDemo
    └─voice-demo
        ├─Constant
        ├─HiddenNumberSmsClientDemo
        ├─HiddenNumCallReceiveServlet
        ├─HiddenNumReceiveMain
        ├─HiddenNumReportReceiveServlet
        ├─VoiceSmsClientDemo
        ├─VoiceSmsFileReceiveServlet
        ├─VoiceSmsReceiveMain
```

#### ApiClient说明
1. ErrorPhoneDemo 异常手机号检测client调用demo
2. MmsClientDemo 彩信client调用demo
3. ShortUrlDemo  短链接client调用demo
4. SmsClientDemo 短信发送client调用demo
5. SmsTemplateClientDemo  短信模板client调用demo
6. SmsStatisticsClientDemo 短信统计client调用demo
7. SupermsgDemo 超级短信client调用demo
8. HiddenNumberSmsClientDemo 隐号功能client调用demo
9. VoiceSmsClientDemo 语音client调用demo



#### 接受推送说明
1. MmsReportReceiveServlet 接受彩信推送
2. ShortUrlDetailReceiveServlet 接受短地址详情推送
3. SmsDeliverReceiveServlet 接受短信上行推送
4. SmsReportReceiveServlet 接受短信报告推送
5. ExamineReportServlet 审核报告接收服务
6. SendReportServlet 发送状态报告接收服务
7. HiddenNumCallReceiveServlet 接受隐号呼叫推送
8. HiddenNumReportReceiveServlet 接受隐号报告推送
9. VoiceSmsFileReceiveServlet 接受语音文件推送



