package com.dahantc.errorphone;

import com.alibaba.fastjson.JSONObject;
import com.dahantc.errorphone.api.ErrorPhoneClient;
import com.dahantc.errorphone.entity.param.QryPhonePropertyParam;
import com.dahantc.errorphone.entity.param.QryPhoneStatusParam;
import com.dahantc.errorphone.entity.response.QryPhonePropertyResponse;
import com.dahantc.errorphone.entity.response.QryPhoneStatusResponse;

/**
 * 类描述：异常号码检测模块SDK调用示例
 * @author 8522
 * @date 2022-05-10 13:52:30
 * 版权所有 Copyright www.dahantc.com
 */
public class ErrorPhoneDemo {
    private final static String ACCOUNT = "dhst8526";
    private final static String PASSWORD = "bb43a2c4081bec02fca7b72f38e63021";
    private final static String URL = "http://www.dh3t.com";

    private final static ErrorPhoneClient errorPhoneClient = new ErrorPhoneClient(URL);

    /**
     * 方法描述：号码状态查询
     *
     * @throws
     * @date 2022-05-09 17:12:02
     */
    public static void qryPhoneStatus(){
        QryPhoneStatusParam qryPhoneStatusParam = new QryPhoneStatusParam();
        qryPhoneStatusParam.setAccount(ACCOUNT);
        qryPhoneStatusParam.setPassword(PASSWORD);
        qryPhoneStatusParam.setPhones("15236465812,15952755460");

        System.out.println("入参：" + JSONObject.toJSONString(qryPhoneStatusParam));
        QryPhoneStatusResponse qryPhoneStatusResponse = errorPhoneClient.qryPhoneStatus(qryPhoneStatusParam);
        System.out.println("返参：" + JSONObject.toJSONString(qryPhoneStatusResponse));
    }

    /**
     * 方法描述：号码属性查询
     *
     * @throws
     * @date 2022-05-09 17:11:50
     */
    public static void qryPhoneProperty(){
        QryPhonePropertyParam qryPhonePropertyParam = new QryPhonePropertyParam();
        qryPhonePropertyParam.setAccount(ACCOUNT);
        qryPhonePropertyParam.setPassword(PASSWORD);
        qryPhonePropertyParam.setPhones("15236465812,15952755460");

        System.out.println("入参：" + JSONObject.toJSONString(qryPhonePropertyParam));
        QryPhonePropertyResponse qryPhonePropertyResponse = errorPhoneClient.qryPhoneProperty(qryPhonePropertyParam);
        System.out.println("返参：" + JSONObject.toJSONString(qryPhonePropertyResponse));
    }

    public static void main(String[] args) {
        //号码状态查询
        qryPhoneStatus();

        //号码属性查询
        qryPhoneProperty();
    }
}
