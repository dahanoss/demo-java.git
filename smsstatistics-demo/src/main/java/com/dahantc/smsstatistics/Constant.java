package com.dahantc.smsstatistics;

import com.dahantc.ctc.utils.MD5;

/**
 * 类描述：账号
 *
 * @author 8526
 * @date 2022-05-10 13:53:17
 * 版权所有 Copyright www.dahantc.com
 */
public class Constant {
    public static String ACCOUNT = "dhst8526";

    public static String PASSWORD = MD5.MD5Encode("456.com");

    /**
     * 调用地址
     */
    public static String REMOTE_URL = "http://www.dh3t.com";
}
