package com.dahantc.smsstatistics;

import com.dahantc.statistics.api.StatisticsClient;
import com.dahantc.statistics.entity.param.SmsStatisticsParam;
import com.dahantc.statistics.entity.response.SmsStatisticsResponse;

/**
 * 类描述：短信统计demo
 *
 * @author 8526
 * @date 2022-05-10 13:51:27
 * 版权所有 Copyright www.dahantc.com
 */
public class SmsStatisticsClientDemo {

    public static void main(String[] args) {
        smsStatisticDemo();
    }

    /**
     * 短信统计调用 demo
     */
    public static void smsStatisticDemo() {
        System.out.println("短信统计调用");
        StatisticsClient client = new StatisticsClient(Constant.REMOTE_URL);
        SmsStatisticsParam param = new SmsStatisticsParam();
        param.setAccount(Constant.ACCOUNT);
        param.setPassword(Constant.PASSWORD);
        param.setStarttime("20220101");
        param.setEndtime("20220505");
        param.setMessagetype("1");
        param.setTjtype("1");
        SmsStatisticsResponse response = client.smsStatistics(param);
        System.out.println("短信统计响应" + response);
    }
}
