package com.dahantc.shorturl;


import com.dahantc.shorturl.api.ShortUrlClient;
import com.dahantc.shorturl.entity.param.ShortUrlGet4WechatAppletParam;
import com.dahantc.shorturl.entity.param.ShortUrlGetParam;
import com.dahantc.shorturl.entity.param.ShortUrlSendParam;
import com.dahantc.shorturl.entity.response.ShortUrlGet4AppletResponse;
import com.dahantc.shorturl.entity.response.ShortUrlGetResponse;
import com.dahantc.shorturl.entity.response.ShortUrlSendResponse;

import java.io.File;

/**
 * 类描述：短链接调用demo
 *
 * @author 8526
 * @date 2022-05-09 15:56:27
 * 版权所有 Copyright www.dahantc.com
 */
public class ShortUrlDemo {


    public static void main(String[] args) {
        shortUrlSend();
        System.out.println("——————————————————————————————————————");
        shortUrlGet();
        System.out.println("——————————————————————————————————————");
        shortUrlGet4Applet();
    }

    private static ShortUrlClient client = new ShortUrlClient(Constant.REMOTE_URL);

    /**
     * 短链接发送功能调用示例
     */
    public static void shortUrlSend() {
        System.out.println("短链接发送调用");
        ShortUrlSendParam param = new ShortUrlSendParam();
        param.setDays(5);
        param.setUrl("https://www.baidu.com");
        param.setAccount(Constant.ACCOUNT);
        param.setPassword(Constant.PASSWORD);
        param.setSign("【大汉三通】");
        param.setContent("测试内容,快点击https://www.baidu.com");
        param.setPhonesFile(new File("C:\\temp\\p.zip"));
        ShortUrlSendResponse response = client.shortUrlSend(param);
        System.out.println("短链接发送响应结果" + response);
    }


    /**
     * 短链接获功能取调用示例
     */
    public static void shortUrlGet() {
        System.out.println("短链接获取");
        ShortUrlGetParam param = new ShortUrlGetParam();
        param.setDays(5);
        param.setUrl("https://www.baidu.com");
        param.setAccount(Constant.ACCOUNT);
        param.setPassword(Constant.PASSWORD);
        ShortUrlGetResponse response = client.shortUrlGet(param);
        System.out.println("短链接获取响应结果" + response);
    }


    /**
     * 生成跳转微信小程序短地址调用示例
     */
    public static void shortUrlGet4Applet() {
        System.out.println("微信小程序短链接获取");
        ShortUrlGet4WechatAppletParam param = new ShortUrlGet4WechatAppletParam();
        param.setDays(5);
        param.setUrl("https://www.baidu.com");
        param.setWxImgUrl("http://images/sss");
        param.setAccount(Constant.ACCOUNT);
        param.setPassword(Constant.PASSWORD);
        ShortUrlGet4AppletResponse response = client.shortUrlGet4WechatApplet(param);
        System.out.println("微信小程序短链接获取响应结果" + response);
    }
}
