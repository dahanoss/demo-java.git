package com.dahantc.shorturl;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

/**
 * 类描述：报告推送-接收示例
 *
 * @author 8526
 * @date 2022-05-10 10:23:28
 * 版权所有 Copyright www.dahantc.com
 */
public class ShortUrlDetailReceiveMain {

    public static class MyThread extends Thread {

        private Server server;

        MyThread(Server server) {
            this.server = server;
        }

        @Override
        public void run() {
            try {
                server.start();
                server.join();
            } catch (Exception e) {
                System.out.println("启动jetty服务失败：" + e);
            }
        }
    }

    public static void main(String[] args) throws Exception {
        //3.1.4.	获取短链接地址详情推送
        Server receiveShortUrl = new Server(8080);
        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setContextPath("/");
        receiveShortUrl.setHandler(context);
        //http://localhost:8080/shortUrlReceive
        context.addServlet(new ServletHolder(new ShortUrlDetailReceiveServlet()), "/shortUrlReceive");
        new MyThread(receiveShortUrl).start();

    }
}
