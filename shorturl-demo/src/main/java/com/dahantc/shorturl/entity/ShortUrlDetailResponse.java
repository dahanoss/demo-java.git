package com.dahantc.shorturl.entity;

import java.util.List;

/**
 * 类描述：短链接详情响应
 *
 * @author 8526
 * @date 2022-05-10 10:37:53
 * 版权所有 Copyright www.dahantc.com
 */
public class ShortUrlDetailResponse {

    private String account;

    private List<ShortUrlDetailData> shorturlDetails;

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public List<ShortUrlDetailData> getShorturlDetails() {
        return shorturlDetails;
    }

    public void setShorturlDetails(List<ShortUrlDetailData> shorturlDetails) {
        this.shorturlDetails = shorturlDetails;
    }

    @Override
    public String toString() {
        return "ShortUrlDetailResponse{" +
                "account='" + account + '\'' +
                ", shorturlDetails=" + shorturlDetails +
                '}';
    }
}
