package com.dahantc.shorturl.entity;

/**
 * 类描述：短链接详情响应
 *
 * @author 8526
 * @date 2022-05-10 10:37:53
 * 版权所有 Copyright www.dahantc.com
 */
public class ShortUrlDetailData {

    /**
     * 访问的浏览器
     */
    private String browser;

    /**
     * 点击次数
     */
    private Long clicknum;

    /**
     * 设备名称
     */
    private String device;

    /**
     * 地址有效期
     */
    private String expirydate;

    /**
     * IP地址
     */
    private String ip;

    /**
     * 操作系统
     */
    private String os;


    /**
     * 点击手机号
     */
    private String phone;

    /**
     * 短地址
     */
    private String shorturl;

    /**
     * 短链接发送任务编号
     */
    private String taskid;

    /**
     * 源地址
     */
    private String url;

    /**
     * 点击日期
     */
    private String wtime;


    public String getBrowser() {
        return browser;
    }

    public void setBrowser(String browser) {
        this.browser = browser;
    }

    public Long getClicknum() {
        return clicknum;
    }

    public void setClicknum(Long clicknum) {
        this.clicknum = clicknum;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getExpirydate() {
        return expirydate;
    }

    public void setExpirydate(String expirydate) {
        this.expirydate = expirydate;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getShorturl() {
        return shorturl;
    }

    public void setShorturl(String shorturl) {
        this.shorturl = shorturl;
    }

    public String getTaskid() {
        return taskid;
    }

    public void setTaskid(String taskid) {
        this.taskid = taskid;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getWtime() {
        return wtime;
    }

    public void setWtime(String wtime) {
        this.wtime = wtime;
    }

    @Override
    public String toString() {
        return "ShortUrlDetailData{" +
                "browser='" + browser + '\'' +
                ", clicknum=" + clicknum +
                ", device='" + device + '\'' +
                ", expirydate='" + expirydate + '\'' +
                ", ip='" + ip + '\'' +
                ", os='" + os + '\'' +
                ", phone='" + phone + '\'' +
                ", shorturl='" + shorturl + '\'' +
                ", taskid='" + taskid + '\'' +
                ", url='" + url + '\'' +
                ", wtime='" + wtime + '\'' +
                '}';
    }
}
