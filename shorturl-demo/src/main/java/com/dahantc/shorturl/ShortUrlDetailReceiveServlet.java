package com.dahantc.shorturl;

import com.alibaba.fastjson.JSON;
import com.dahantc.shorturl.entity.ShortUrlDetailResponse;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * 类描述：3.1.4.	推送短信状态报告
 *
 * @author 8522
 * @date 2022-04-29 15:23:28
 * 版权所有 Copyright www.dahantc.com
 */
public class ShortUrlDetailReceiveServlet extends HttpServlet {
    private static final long serialVersionUID = 252700719588234609L;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String status = "failure";
        Map<String, String> map = new HashMap<>();
        try {
            String shortUrl = request.getParameter("shorturl");
            ShortUrlDetailResponse detailResponse = JSON.parseObject(shortUrl, ShortUrlDetailResponse.class);
            System.out.println("收到响应结果：" + detailResponse);
            status = "success";
        } catch (Exception e) {
            System.out.println("推送解析异常：" + e.getMessage());
        }
        map.put("status", status);
        PrintWriter writer = response.getWriter();
        writer.print(JSON.toJSONString(map));
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        try (PrintWriter out = response.getWriter()) {
            out.print("Please use  POST method to submit data");
        } catch (Exception e) {
        }
    }

}
