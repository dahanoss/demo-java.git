package com.dahantc.voice.entity;

/**
 * 类描述：语音文件详情数据
 *
 * @author 8526
 * @date 2022-05-11 9:14:04
 * 版权所有 Copyright www.dahantc.com
 */
public class VoiceSmsFileData {

    /**
     * 客户账号
     */
    private String account;

    /**
     * 语音文件标识id
     */
    private String mfid;

    /**
     * 语音文件状态，0：文件被删除，1：审核通过，2：审核驳回，3：审核失败
     */
    private String status;

    /**
     * 审核说明
     */
    private String remark;

    /**
     * 审核时间，格式：yyyy-MM-dd HH:mm:ss
     */
    private String auditTime;


    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getMfid() {
        return mfid;
    }

    public void setMfid(String mfid) {
        this.mfid = mfid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getAuditTime() {
        return auditTime;
    }

    public void setAuditTime(String auditTime) {
        this.auditTime = auditTime;
    }

    @Override
    public String toString() {
        return "VoiceSmsFileData{" +
                "account='" + account + '\'' +
                ", mfid='" + mfid + '\'' +
                ", status='" + status + '\'' +
                ", remark='" + remark + '\'' +
                ", auditTime='" + auditTime + '\'' +
                '}';
    }
}
