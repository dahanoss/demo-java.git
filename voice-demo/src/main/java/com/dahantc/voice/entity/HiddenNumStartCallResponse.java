package com.dahantc.voice.entity;

import java.util.List;

/**
 * 类描述：隐号起始呼叫推送数据
 *
 * @author 8526
 * @date 2022-05-11 9:33:01
 * 版权所有 Copyright www.dahantc.com
 */
public class HiddenNumStartCallResponse {

    private List<HiddenNumStartCallData> data;

    public List<HiddenNumStartCallData> getData() {
        return data;
    }

    public void setData(List<HiddenNumStartCallData> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "HiddenNumStartCallResponse{" +
                "data=" + data +
                '}';
    }
}
