package com.dahantc.voice.entity;

import java.util.List;

/**
 * 类描述：语音文件响应
 *
 * @author 8526
 * @date 2022-05-11 9:12:44
 * 版权所有 Copyright www.dahantc.com
 */
public class VoiceSmsFileResponse {

    /**
     * 请求结果
     */
    private String result;

    /**
     * 描述
     */
    private String desc;


    private List<VoiceSmsFileData> data;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public List<VoiceSmsFileData> getData() {
        return data;
    }

    public void setData(List<VoiceSmsFileData> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "VoiceSmsFileResponse{" +
                "result='" + result + '\'' +
                ", desc='" + desc + '\'' +
                ", data=" + data +
                '}';
    }
}
