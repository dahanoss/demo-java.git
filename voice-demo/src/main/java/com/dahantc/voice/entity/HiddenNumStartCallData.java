package com.dahantc.voice.entity;

/**
 * 类描述：隐号起始呼叫详情数据
 *
 * @author 8526
 * @date 2022-05-11 9:34:09
 * 版权所有 Copyright www.dahantc.com
 */
public class HiddenNumStartCallData {


    /**
     * 请求号码时，所带msgid
     */
    private String msgid;

    /**
     * 区分推送话单唯一性
     */
    private String callid;

    /**
     * 被叫来显号码
     */
    private String calldisplay;

    /**
     * 发起呼叫时间，格式：yyyy-mm-dd hh:mm:ss
     */
    private String calltime;

    /**
     * 真实号码
     */
    private String telA;

    /**
     * 对端号码
     */
    private String telB;

    /**
     * 录音控制, 0：不录音 1：接通后录音 2：被叫响铃后录音
     */
    private String callrecording;

    /**
     * 呼叫类型：
     * 10：通话主叫
     * 11：通话被叫
     * 12：短信发送
     * 13：短信接收
     * 20：呼叫不允许
     * 21: 未开户不允许
     * 30：短信不允许
     * 31：转接短信
     * 126: 双呼
     */
    private String calltype;


    public String getMsgid() {
        return msgid;
    }

    public void setMsgid(String msgid) {
        this.msgid = msgid;
    }

    public String getCallid() {
        return callid;
    }

    public void setCallid(String callid) {
        this.callid = callid;
    }

    public String getCalldisplay() {
        return calldisplay;
    }

    public void setCalldisplay(String calldisplay) {
        this.calldisplay = calldisplay;
    }

    public String getCalltime() {
        return calltime;
    }

    public void setCalltime(String calltime) {
        this.calltime = calltime;
    }

    public String getTelA() {
        return telA;
    }

    public void setTelA(String telA) {
        this.telA = telA;
    }

    public String getTelB() {
        return telB;
    }

    public void setTelB(String telB) {
        this.telB = telB;
    }

    public String getCallrecording() {
        return callrecording;
    }

    public void setCallrecording(String callrecording) {
        this.callrecording = callrecording;
    }

    public String getCalltype() {
        return calltype;
    }

    public void setCalltype(String calltype) {
        this.calltype = calltype;
    }


    @Override
    public String toString() {
        return "HiddenNumStartCallData{" +
                "msgid='" + msgid + '\'' +
                ", callid='" + callid + '\'' +
                ", calldisplay='" + calldisplay + '\'' +
                ", calltime='" + calltime + '\'' +
                ", telA='" + telA + '\'' +
                ", telB='" + telB + '\'' +
                ", callrecording='" + callrecording + '\'' +
                ", calltype='" + calltype + '\'' +
                '}';
    }
}
