package com.dahantc.voice;

import com.dahantc.oss.api.HiddenNumberSmsClient;
import com.dahantc.oss.entity.param.hidden.*;
import com.dahantc.oss.entity.response.hidden.*;

/**
 * 类描述：隐号功能demo
 *
 * @author 8526
 * @date 2022-05-10 14:13:34
 * 版权所有 Copyright www.dahantc.com
 */
public class HiddenNumberSmsClientDemo {

    public static void main(String[] args) {
        getPhoneAXBDemo();
        System.out.println("--------------------");
        getPhoneAXNDemo();
        System.out.println("--------------------");
        getPhoneAXYBDemo();
        System.out.println("--------------------");
        getPhoneAXxYBDemo();
        System.out.println("--------------------");
        getReportDemo();
        System.out.println("--------------------");
        numberReleaseDemo();
    }

    private static HiddenNumberSmsClient client = new HiddenNumberSmsClient(Constant.REMOTE_URL);

    /**
     * 提供一个号码，让A手机可以通过X号码和B进行呼叫,B手机可以通过X号码和A进行呼叫
     * 调用demo
     */
    public static void getPhoneAXBDemo() {
        System.out.println("隐号呼叫AXB调用");
        PhoneAXBParam param = new PhoneAXBParam();
        param.setAccount(Constant.ACCOUNT);
        param.setPassword(Constant.PASSWORD);
        param.setPhoneA("1771502****");
        param.setPhoneB("1771503****");
        param.setExpiration(200000);
        PhoneAXBResponse phoneAXB = client.getPhoneAXB(param);
        System.out.println("隐号呼叫AXB响应" + phoneAXB);
    }

    public static void getPhoneAXNDemo() {
        System.out.println("隐号呼叫AXB调用");
        PhoneAXNParam param = new PhoneAXNParam();
        param.setAccount(Constant.ACCOUNT);
        param.setPassword(Constant.PASSWORD);
        param.setPhoneA("1771502****");
        param.setExpiration(200000);
        PhoneAXNResponse response = client.getPhoneAXN(param);
        System.out.println("隐号呼叫AXB响应" + response);
    }


    public static void getPhoneAXYBDemo() {
        System.out.println("隐号呼叫AXYB调用");
        PhoneAXYBParam param = new PhoneAXYBParam();
        param.setAccount(Constant.ACCOUNT);
        param.setPassword(Constant.PASSWORD);
        param.setPhoneA("1771502****");
        param.setPhoneX("1771502****");
        param.setPhoneB("1771502****");
        param.setExpiration(200000);
        PhoneAXYBResponse response = client.getPhoneAXYB(param);
        System.out.println("隐号呼叫AXYB响应" + response);
    }

    public static void getPhoneAXxYBDemo() {
        System.out.println("隐号呼叫AXxYB调用");
        PhoneAXxYBParam param = new PhoneAXxYBParam();
        param.setAccount(Constant.ACCOUNT);
        param.setPassword(Constant.PASSWORD);
        param.setPhoneA("1771502****");
        param.setPhoneX("1771502****");
        param.setPhoneXx("1771502****");
        param.setExpiration(200000);
        param.setAybExpiration(300);
        param.setCalldisplay("0");
        param.setAybshowx("0");
        PhoneAXxYBResponse response = client.getPhoneAXxYB(param);
        System.out.println("隐号呼叫AXxYB响应" + response);
    }


    /**
     * 隐号报告查询demo
     */
    public static void getReportDemo() {
        System.out.println("隐号报告查询调用");
        PhoneReportParam param = new PhoneReportParam();
        param.setAccount(Constant.ACCOUNT);
        param.setPassword(Constant.PASSWORD);
        PhoneReportResponse response = client.getReport(param);
        System.out.println("隐号报告查询响应" + response);
    }

    /**
     * 隐号号码释放demo
     */
    public static void numberReleaseDemo() {
        System.out.println("隐号号码释放调用");
        PhoneReleaseParam param = new PhoneReleaseParam();
        param.setAccount(Constant.ACCOUNT);
        param.setPassword(Constant.PASSWORD);
        param.setDeletetype("1");
        param.setPhone("111177777,");
        PhoneReleaseResponse response = client.numberDelete(param);
        System.out.println("隐号号码释放响应" + response);
    }
}
