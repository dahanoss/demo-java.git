package com.dahantc.voice;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

/**
 * 类描述：语音短信clientdemo
 *
 * @author 8526
 * @date 2022-05-10 14:12:45
 * 版权所有 Copyright www.dahantc.com
 */
public class VoiceSmsReceiveMain {
    public static class MyThread extends Thread {

        private Server server;

        MyThread(Server server) {
            this.server = server;
        }

        @Override
        public void run() {
            try {
                server.start();
                server.join();
            } catch (Exception e) {
                System.out.println("启动jetty服务失败：" + e);
            }
        }
    }

    /**
     * 启动服务 接受语音文件推送
     *
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {

        Server receiveServer = new Server(8080);
        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setContextPath("/");
        receiveServer.setHandler(context);
        //彩信报告推送  http://localhost:8080/voiceSmsFileReceive
        context.addServlet(new ServletHolder(new VoiceSmsFileReceiveServlet()), "/voiceSmsFileReceive");
        new MyThread(receiveServer).start();
    }

}
