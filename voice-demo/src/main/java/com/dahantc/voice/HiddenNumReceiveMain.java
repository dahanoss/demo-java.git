package com.dahantc.voice;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

/**
 * 类描述：隐号接受推送demo
 *
 * @author 8526
 * @date 2022-05-11 09:35:45
 * 版权所有 Copyright www.dahantc.com
 */
public class HiddenNumReceiveMain {
    public static class MyThread extends Thread {

        private Server server;

        MyThread(Server server) {
            this.server = server;
        }

        @Override
        public void run() {
            try {
                server.start();
                server.join();
            } catch (Exception e) {
                System.out.println("启动jetty服务失败：" + e);
            }
        }
    }

    /**
     * 启动服务 接受隐号状态推送和隐号起始呼叫推送
     *
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {

        Server receiveServer = new Server(8080);
        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setContextPath("/");
        receiveServer.setHandler(context);
        //隐号呼叫接受推送  http://localhost:8080/hiddenNumCallReceive
        context.addServlet(new ServletHolder(new HiddenNumCallReceiveServlet()), "/hiddenNumCallReceive");
        //隐号报告接受推送  http://localhost:8080/hiddenNumReportReceive
        context.addServlet(new ServletHolder(new HiddenNumReportReceiveServlet()), "/hiddenNumReportReceive");
        new MyThread(receiveServer).start();
    }

}
