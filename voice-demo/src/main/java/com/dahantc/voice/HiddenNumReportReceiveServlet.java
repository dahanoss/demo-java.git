package com.dahantc.voice;

import com.alibaba.fastjson.JSON;
import com.dahantc.oss.entity.response.hidden.PhoneReportResponse;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * 类描述：隐号报告推送处理
 *
 * @author 8526
 * @date 2022-05-11 9:07:36
 * 版权所有 Copyright www.dahantc.com
 */
public class HiddenNumReportReceiveServlet extends HttpServlet {

    private static final long serialVersionUID = 252700719588234609L;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String status = "failure";
        Map<String, String> map = new HashMap<>();
        try {
            String voicereport = request.getParameter("voicereport");
            PhoneReportResponse phoneReportResponse = JSON.parseObject(voicereport, PhoneReportResponse.class);
            System.out.println("收到响应结果：" + phoneReportResponse);
            status = "success";
        } catch (Exception e) {
            System.out.println("推送解析异常：" + e.getMessage());
        }
        map.put("status", status);
        PrintWriter writer = response.getWriter();
        writer.print(JSON.toJSONString(map));
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        try (PrintWriter out = response.getWriter()) {
            out.print("Please use  POST method to submit data");
        } catch (Exception e) {
        }
    }


}
