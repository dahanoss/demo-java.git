package com.dahantc.voice;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.dahantc.ctc.utils.XMLUtil;
import com.dahantc.voice.entity.VoiceSmsFileResponse;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

/**
 * 类描述：语音文件推送接受
 *
 * @author 8526
 * @date 2022-05-11 9:07:36
 * 版权所有 Copyright www.dahantc.com
 */
public class VoiceSmsFileReceiveServlet extends HttpServlet {

    private static final long serialVersionUID = 252700719588234609L;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String status = "failure";
        Map<String, String> map = new HashMap<>();
        try {
            JSONObject jsonObject = getRequestData(request);
            VoiceSmsFileResponse voiceSmsFileResponse = JSONObject.toJavaObject(jsonObject, VoiceSmsFileResponse.class);
            System.out.println("收到响应结果：" + voiceSmsFileResponse);
            status = "success";
        } catch (Exception e) {
            System.out.println("推送解析异常：" + e.getMessage());
        }
        map.put("status", status);
        PrintWriter writer = response.getWriter();
        writer.print(JSON.toJSONString(map));
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        try (PrintWriter out = response.getWriter()) {
            out.print("Please use  POST method to submit data");
        } catch (Exception e) {
        }
    }

    /**
     * 方法描述：获取入参转化为JSONObject对象
     * @param request
     * @return {@link JSONObject}
     * @date 2022-04-29 16:00:04
     */
    protected JSONObject getRequestData(HttpServletRequest request) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        try {
            InputStream inputStream = request.getInputStream();
            byte[] by = new byte[1024];
            int len;
            if (inputStream != null) {
                while ((len = inputStream.read(by)) != -1) {
                    outputStream.write(by, 0, len);
                }
                outputStream.flush();
            }
            return JSONObject.parseObject(new String(outputStream.toByteArray(), StandardCharsets.UTF_8));
        } catch (IOException e) {
            System.out.println("入参转化为JSONObject对象失败：" + e);
            try {
                outputStream.close();
            } catch (Exception e1) {
                System.out.println("入参转化为JSONObject对象失败：" + e1);
            }
        }
        return null;
    }

}
