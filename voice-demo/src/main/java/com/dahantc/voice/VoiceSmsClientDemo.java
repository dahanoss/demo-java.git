package com.dahantc.voice;

import com.dahantc.oss.api.VoiceSmsClient;
import com.dahantc.oss.entity.param.voice.CaptchaVoiceData;
import com.dahantc.oss.entity.param.voice.CaptchaVoiceParam;
import com.dahantc.oss.entity.param.voice.MediaCaptchaVoiceData;
import com.dahantc.oss.entity.param.voice.MediaCaptchaVoiceParam;
import com.dahantc.oss.entity.param.voice.MediaVoiceData;
import com.dahantc.oss.entity.param.voice.MediaVoiceParam;
import com.dahantc.oss.entity.param.voice.MixVoiceData;
import com.dahantc.oss.entity.param.voice.MixVoiceParam;
import com.dahantc.oss.entity.param.voice.TextVoiceData;
import com.dahantc.oss.entity.param.voice.TextVoiceParam;
import com.dahantc.oss.entity.param.voice.VocMediaUploadParam;
import com.dahantc.oss.entity.response.voice.VocUploadResponse;
import com.dahantc.oss.entity.response.voice.VoiceSubmitResponse;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;

/**
 * 类描述：语音短信clientdemo
 * @author 8526
 * @date 2022-05-10 14:12:45
 * 版权所有 Copyright www.dahantc.com
 */
public class VoiceSmsClientDemo {

    public static void main(String[] args) {
        captchaSubmitDemo();
        System.out.println("---------------------------");
        mediaSubmitDemo();
        System.out.println("---------------------------");
        textSubmitDemo();
        System.out.println("---------------------------");
        mixSubmitDemo();
        System.out.println("---------------------------");
        mediaCaptchaSubmitDemo();
        System.out.println("---------------------------");
        mediaVoiceUploadDemo();
    }

    private static VoiceSmsClient client = new VoiceSmsClient(Constant.REMOTE_URL);

    public static void captchaSubmitDemo() {
        System.out.println("验证码提交调用");
        CaptchaVoiceParam mediaVoiceParam = new CaptchaVoiceParam();
        mediaVoiceParam.setAccount(Constant.ACCOUNT);
        mediaVoiceParam.setPassword(Constant.PASSWORD);
        CaptchaVoiceData voiceData = new CaptchaVoiceData.CaptchaVoiceDataBuilder()
                .callee("157****6131")
                .msgid("49a5f1d78ab84b90a963beac04867888")
                .calltype(1)
                .playmode(0)
                .build();
        List<CaptchaVoiceData> data = Arrays.asList(voiceData);
        mediaVoiceParam.setData(data);
        VoiceSubmitResponse voiceSubmitResponse = client.captchaSubmit(mediaVoiceParam);
        System.out.println("验证码提交响应" + voiceSubmitResponse);
    }

    public static void textSubmitDemo() {
        System.out.println("文本外呼提交调用");
        TextVoiceParam mediaVoiceParam = new TextVoiceParam();
        mediaVoiceParam.setAccount(Constant.ACCOUNT);
        mediaVoiceParam.setPassword(Constant.PASSWORD);
        TextVoiceData voiceData = new TextVoiceData.TextVoiceDataBuilder()
                .callee("157****6131")
                .msgid("49a5f1d78ab84b90a963beac04867888")
                .text("这是一条TTS的测试消息")
                .calltype(0)
                .playmode(0)
                .build();
        List<TextVoiceData> data = Arrays.asList(voiceData);
        mediaVoiceParam.setData(data);
        VoiceSubmitResponse voiceSubmitResponse = client.textSubmit(mediaVoiceParam);
        System.out.println("文本外呼提交响应" + voiceSubmitResponse);
    }

    public static void mixSubmitDemo() {
        System.out.println("混合呼叫提交调用");
        MixVoiceParam mediaVoiceParam = new MixVoiceParam();
        mediaVoiceParam.setAccount(Constant.ACCOUNT);
        mediaVoiceParam.setPassword(Constant.PASSWORD);
        MixVoiceData voiceData = new MixVoiceData.MixVoiceDataBuilder()
                .callee("157****6131")
                .msgid("49a5f1d78ab84b90a963beac04867888")
                .text("这是一条TTS的测试消息")
                .medianame("a81dfae08b4b478cbc266d77aefbbb03")
                .calltype(3)
                .playmode(2)
                .build();
        List<MixVoiceData> data = Arrays.asList(voiceData);
        mediaVoiceParam.setData(data);
        VoiceSubmitResponse voiceSubmitResponse = client.mixSubmit(mediaVoiceParam);
        System.out.println("混合呼叫响应" + voiceSubmitResponse);
    }

    public static void mediaCaptchaSubmitDemo() {
        System.out.println("语音文件验证码提交调用");
        MediaCaptchaVoiceParam mediaVoiceParam = new MediaCaptchaVoiceParam();
        mediaVoiceParam.setAccount(Constant.ACCOUNT);
        mediaVoiceParam.setPassword(Constant.PASSWORD);
        MediaCaptchaVoiceData voiceData = new MediaCaptchaVoiceData.MediaVoiceDataBuilder()
                .callee("157****6131")
                .msgid("49a5f1d78ab84b90a963beac04867888")
                .medianame("myverifycodefilename_1")
                .text("255178")
                .calltype(1)
                .playmode(3)
                .build();
        List<MediaCaptchaVoiceData> data = Arrays.asList(voiceData);
        mediaVoiceParam.setData(data);
        VoiceSubmitResponse voiceSubmitResponse = client.mediaCaptchaSubmit(mediaVoiceParam);
        System.out.println("语音文件验证码外呼响应" + voiceSubmitResponse);
    }

    public static void mediaSubmitDemo() {
        System.out.println("语音文件外呼提交调用");
        MediaVoiceParam mediaVoiceParam = new MediaVoiceParam();
        mediaVoiceParam.setAccount(Constant.ACCOUNT);
        mediaVoiceParam.setPassword(Constant.PASSWORD);
        MediaVoiceData voiceData = new MediaVoiceData.MediaVoiceDataBuilder()
                .callee("157****6131")
                .msgid("49a5f1d78ab84b90a963beac04867888")
                .calltype(2)
                .playmode(1)
                .medianame("a81dfae08b4b478cbc266d77aefbbb01;a81dfae08b4b478cbc266d77aefbbb02")
                .build();
        List<MediaVoiceData> data = Arrays.asList(voiceData);
        mediaVoiceParam.setData(data);
        VoiceSubmitResponse voiceSubmitResponse = client.mediaSubmit(mediaVoiceParam);
        System.out.println("语音文件外呼响应" + voiceSubmitResponse);
    }

    public static void mediaVoiceUploadDemo() {
        System.out.println("语音文件上传调用");
        VocMediaUploadParam param = new VocMediaUploadParam();
        param.setAccount(Constant.ACCOUNT);
        param.setPassword(Constant.PASSWORD);
        param.setFilename("w.wav");
        File file = new File("c:\\temp\\1.wav");
        int _bufferSize = (int) file.length();
        //定义buffer缓冲区大小
        byte[] buffer = new byte[_bufferSize];
        FileInputStream in = null;
        try {
            in = new FileInputStream(file);
            int len = 0;
            if ((len = in.available()) <= buffer.length) {
                in.read(buffer, 0, len);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        String base64Str = Base64.getEncoder().encodeToString(buffer);
        param.setFiledata(base64Str);
        VocUploadResponse response = client.vocMediaUpload(param);
        System.out.println("语音文件上传响应" + response);
    }


}
