package com.dahantc.mms;

import com.dahantc.mms.api.MmsSendClient;
import com.dahantc.mms.entity.param.*;
import com.dahantc.mms.entity.response.MmsReportResponse;
import com.dahantc.mms.entity.response.MmsSendResponse;

import java.util.Arrays;

/**
 * 类描述：彩信demo
 *
 * @author 8526
 * @date 2022-05-10 13:55:28
 * 版权所有 Copyright www.dahantc.com
 */
public class MmsClientDemo {

    public static void main(String[] args) {
        mmsSubmitDemo();
        System.out.println("-------------------");
        mmsReportDemo();
    }

    private static MmsSendClient client = new MmsSendClient(Constant.REMOTE_URL);

    /**
     * 彩信提交调用 demo
     */
    public static void mmsSubmitDemo() {
        System.out.println("彩信提交调用");
        MmsHead mmsSendHead = new MmsHead("002", Constant.ACCOUNT, Constant.PASSWORD);
        MmsSendBody body = new MmsSendBody(Arrays.asList(new SubmitMsg("17777777777", "标题", "1", "12", "23"),new SubmitMsg("17777777777", "标题1", "1", "12", "23")));
        MmsSendParam mmsSendParam = new MmsSendParam(mmsSendHead, body);
        MmsSendResponse mmsSendResponse = client.mmsSend(mmsSendParam);
        System.out.println("彩信提交响应" + mmsSendResponse);
    }

    /**
     * 彩信报告查询调用 demo
     */
    public static void mmsReportDemo() {
        System.out.println("彩信报告查询调用");
        MmsHead mmsSendHead = new MmsHead("004", Constant.ACCOUNT, Constant.PASSWORD);
        MmsReportParam param = new MmsReportParam(mmsSendHead);
        MmsReportResponse reportResponse = client.queryReport(param);
        System.out.println("彩信报告查询响应" + reportResponse);
    }
}
