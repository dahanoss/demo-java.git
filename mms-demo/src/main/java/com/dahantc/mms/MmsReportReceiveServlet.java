package com.dahantc.mms;

import com.alibaba.fastjson.JSON;
import com.dahantc.ctc.utils.XMLUtil;
import com.dahantc.mms.entity.response.MmsReportResponse;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * 类描述：彩信报告推送
 *
 * @author 8526
 * @date 2022-05-10 15:23:28
 * 版权所有 Copyright www.dahantc.com
 */
public class MmsReportReceiveServlet extends HttpServlet {
    private static final long serialVersionUID = 252700719588234609L;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String status = "failure";
        Map<String, String> map = new HashMap<>();
        try {
            String mmsreport = request.getParameter("mmsreport");
            MmsReportResponse deliverResponse = (MmsReportResponse)XMLUtil.convertXmlStrToObject(MmsReportResponse.class, mmsreport);
            System.out.println("收到响应结果：" + deliverResponse);
            status = "success";
        } catch (Exception e) {
            System.out.println("推送解析异常：" + e.getMessage());
        }
        map.put("status", status);
        PrintWriter writer = response.getWriter();
        writer.print(JSON.toJSONString(map));
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        try (PrintWriter out = response.getWriter()) {
            out.print("Please use  POST method to submit data");
        } catch (Exception e) {
        }
    }

}
