package com.dahantc.mms;

import com.dahantc.ctc.utils.MD5;

/**
 * 类描述：账号类
 *
 * @author 8526
 * @date 2022-05-10 10:07:06
 * 版权所有 Copyright www.dahantc.com
 */
public class Constant {

    public static String ACCOUNT = "dhst8526";

    public static String PASSWORD = MD5.MD5Encode("456.com");

    /**
     * 调用地址
     */
    public static String REMOTE_URL = "http://www.dh3t.com";
}
