package com.dahantc.mms;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

/**
 * 类描述：彩信报告推送接受示例
 *
 * @author 8526
 * @date 2022-05-10 10:23:28
 * 版权所有 Copyright www.dahantc.com
 */
public class MmsReceiveMain {

    public static class MyThread extends Thread {

        private Server server;

        MyThread(Server server) {
            this.server = server;
        }

        @Override
        public void run() {
            try {
                server.start();
                server.join();
            } catch (Exception e) {
                System.out.println("启动jetty服务失败：" + e);
            }
        }
    }

    /**
     * 启动服务 接受彩信报告推送
     *
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {

        Server receiveServer = new Server(8080);
        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setContextPath("/");
        receiveServer.setHandler(context);
        //彩信报告推送  http://localhost:8080/mmsReportReceive
        context.addServlet(new ServletHolder(new MmsReportReceiveServlet()), "/mmsReportReceive");
        new MyThread(receiveServer).start();
    }
}
