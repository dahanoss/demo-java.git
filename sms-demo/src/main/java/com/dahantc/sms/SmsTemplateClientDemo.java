package com.dahantc.sms;

import com.dahantc.sms.api.SmsTemplateClient;
import com.dahantc.sms.entity.param.template.SmsTemplateData;
import com.dahantc.sms.entity.param.template.SmsTemplateDelParam;
import com.dahantc.sms.entity.param.template.SmsTemplateShowParam;
import com.dahantc.sms.entity.param.template.SmsTemplateSubmitParam;
import com.dahantc.sms.entity.param.template.SmsTemplateUploadParam;
import com.dahantc.sms.entity.param.template.SmsTemplateVariable;
import com.dahantc.sms.entity.response.template.SmsTemplateDelResponse;
import com.dahantc.sms.entity.response.template.SmsTemplateShowResponse;
import com.dahantc.sms.entity.response.template.SmsTemplateSubmitResponse;
import com.dahantc.sms.entity.response.template.SmsTemplateUploadResponse;

import java.util.LinkedList;

/**
 * 类描述：调用demo
 *
 * @author 8526
 * @date 2022-05-09 18:22:03
 * 版权所有 Copyright www.dahantc.com
 */
public class SmsTemplateClientDemo {

    public static void main(String[] args) {
        templateUploadDemo();
        System.out.println("------------------------");
        templateSubmitDemo();
        System.out.println("------------------------");
        templateQueryDemo();
        System.out.println("------------------------");
        templateDelDemo();
    }

    private static SmsTemplateClient client = new SmsTemplateClient(Constant.REMOTE_URL);

    /**
     * 短信模板上传 demo
     */
    public static void templateUploadDemo() {
        System.out.println("短信模板上传调用");
        SmsTemplateUploadParam param = new SmsTemplateUploadParam();
        param.setAccount(Constant.ACCOUNT);
        param.setPassword(Constant.PASSWORD);
        param.setContent("你好，这是白模板${1,10}示例");
        param.setSign("【大汉三通】");
        param.setRemark("");
        SmsTemplateUploadResponse response = client.templateUpload(param);
        System.out.println("短信模板上传响应" + response);
    }

    /**
     * 使用模板id下发 demo
     */
    public static void templateSubmitDemo() {
        System.out.println("使用模板id下发调用");
        SmsTemplateSubmitParam param = new SmsTemplateSubmitParam();
        param.setAccount(Constant.ACCOUNT);
        param.setPassword(Constant.PASSWORD);
        param.setPhones("1771502****");
        SmsTemplateData smsTemplateData = new SmsTemplateData();
        smsTemplateData.setId("2c9289788092247f0180a765b9fb0002");
        LinkedList<SmsTemplateVariable> variables = new LinkedList<>();
        SmsTemplateVariable smsTemplateVariable = new SmsTemplateVariable();
        smsTemplateVariable.setName("1");
        smsTemplateVariable.setValue("a");
        variables.add(smsTemplateVariable);
        smsTemplateData.setVariables(variables);
        param.setTemplate(smsTemplateData);
        SmsTemplateSubmitResponse response = client.templateSubmit(param);
        System.out.println("使用模板id下发响应" + response);
    }

    /**
     * 短信模板查询 demo
     */
    public static void templateQueryDemo() {
        System.out.println("短信模板查询调用");
        SmsTemplateShowParam param = new SmsTemplateShowParam();
        param.setAccount(Constant.ACCOUNT);
        param.setPassword(Constant.PASSWORD);
        param.setTemplateIds("2c9289788092247f0180a765b9fb0002");
        SmsTemplateShowResponse response = client.templateShow(param);
        System.out.println("短信模板查询响应" + response);
    }

    /**
     * 短信模板删除demo
     */
    public static void templateDelDemo() {
        System.out.println("短信模板删除调用");
        SmsTemplateDelParam param = new SmsTemplateDelParam();
        param.setAccount(Constant.ACCOUNT);
        param.setPassword(Constant.PASSWORD);
        param.setTemplateIds("2c9289788092247f0180a765b9fb00021");
        SmsTemplateDelResponse response = client.templateDelete(param);
        System.out.println("短信模板删除响应" + response);
    }
}
