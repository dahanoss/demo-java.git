package com.dahantc.sms;

import com.dahantc.sms.api.SmsSendClient;
import com.dahantc.sms.entity.param.msg.BalanceParam;
import com.dahantc.sms.entity.param.msg.BaseSendData;
import com.dahantc.sms.entity.param.msg.DeliverParam;
import com.dahantc.sms.entity.param.msg.ReportParam;
import com.dahantc.sms.entity.param.msg.SmsSendBatchParam;
import com.dahantc.sms.entity.param.msg.SmsSendParam;
import com.dahantc.sms.entity.param.msg.SmsSendWSignParam;
import com.dahantc.sms.entity.response.BalanceResponse;
import com.dahantc.sms.entity.response.BatchSendResponse;
import com.dahantc.sms.entity.response.DeliverResponse;
import com.dahantc.sms.entity.response.ReportResponse;
import com.dahantc.sms.entity.response.SmsSendResponse;
import com.dahantc.sms.entity.response.SmsSendWSignResponse;

import java.util.Arrays;
import java.util.HashMap;

/**
 * 类描述：调用demo
 * @author 8526
 * @date 2022-05-09 18:22:03
 * 版权所有 Copyright www.dahantc.com
 */
public class SmsClientDemo {


    public static void main(String[] args) {
        smsSendDemo();
        System.out.println("------------------------");
        smsSendSignDemo();
        System.out.println("------------------------");
        smsSendBatchDemo();
        System.out.println("------------------------");
        queryDeliverDemo();
        System.out.println("------------------------");
        queryReportDemo();
        System.out.println("------------------------");
        queryBalanceDemo();
    }

    private static SmsSendClient client = new SmsSendClient(Constant.REMOTE_URL);

    /**
     * 短信发送调用demo
     */
    public static void smsSendDemo() {
        System.out.println("短信发送调用");
        SmsSendParam param = new SmsSendParam.SmsSendBuilder()
                .account(Constant.ACCOUNT)
                .password(Constant.PASSWORD)
                .msgid("2c92825934837c4d0134837dcba00150")
                .phones("1571166****,1571165****")
                .content("您好，您的手机验证码为：430237。")
                .sign("【大汉三通】")
                .subcode("")
                .build();
        SmsSendResponse smsSendResponse = client.sendSms(param);
        System.out.println("短信发送响应" + smsSendResponse);
    }

    /**
     * 短信content发送携带签名发送 demo
     */
    public static void smsSendSignDemo() {
        System.out.println("短信content发送携带签名调用");
        SmsSendWSignParam param = new SmsSendWSignParam.SmsSendSignBuilder()
                .account(Constant.ACCOUNT)
                .password(Constant.PASSWORD)
                .msgid("2c92825934837c4d0134837dcba00150")
                .phones("1771502****")
                .content("【大汉三通】，您的手机验证码为：430237。")
                .subcode("")
                .build();
        SmsSendWSignResponse smsSendWSignResponse = client.sendSmsWithSign(param);
        System.out.println("调用响应" + smsSendWSignResponse);
    }

    /**
     * 不同内容短信批量发送
     */
    public static void smsSendBatchDemo() {
        System.out.println("短信不同内容批量发送调用");
        HashMap<String, Object> params = new HashMap<>();
        params.put("param1", "value1");
        BaseSendData data = new BaseSendData.BaseSendDataBuilder()
                .msgid("2c92825934837c4d0134837dcba00150")
                .phones("w883388")
                .content("【大汉三通】，您的手机验证码为：430237。")
                .params(params)
                .subcode("")
                .build();
        SmsSendBatchParam param = new SmsSendBatchParam(Arrays.asList(data));
        param.setAccount(Constant.ACCOUNT);
        param.setPassword(Constant.PASSWORD);
        param.setCallbackurl("http://callback.com/call");
        BatchSendResponse smsSendResponse = client.sendSmsBatch(param);
        System.out.println("短信不同内容批量发送响应" + smsSendResponse);
    }

    /**
     * 查询上行回复短信
     */
    public static void queryDeliverDemo() {
        System.out.println("查询回复上行短信调用");
        DeliverParam param = new DeliverParam();
        param.setAccount(Constant.ACCOUNT);
        param.setPassword(Constant.PASSWORD);
        DeliverResponse deliverResponse = client.queryDeliver(param);
        System.out.println("查询回复上行短信响应" + deliverResponse);
    }

    /**
     * 获取状态报告
     */
    public static void queryReportDemo() {
        System.out.println("获取状态报告调用");
        ReportParam param = new ReportParam();
        param.setAccount(Constant.ACCOUNT);
        param.setPassword(Constant.PASSWORD);
        ReportResponse reportResponse = client.queryReport(param);
        System.out.println("获取状态报告响应" + reportResponse);
    }

    /**
     * 查询账户余额
     */
    public static void queryBalanceDemo() {
        System.out.println("查询账户余额调用");
        BalanceParam param = new BalanceParam();
        param.setAccount(Constant.ACCOUNT);
        param.setPassword(Constant.PASSWORD);
        BalanceResponse balanceResponse = client.queryBalance(param);
        System.out.println("查询账户余额响应" + balanceResponse);
    }
}
