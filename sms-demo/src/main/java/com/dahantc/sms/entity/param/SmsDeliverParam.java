package com.dahantc.sms.entity.param;

import java.util.List;

/**
 * 类描述：短信上行消息报告接收-参数
 *
 * @author 8526
 * @date 2022-05-10 13:06:36
 * 版权所有 Copyright www.dahantc.com
 */
public class SmsDeliverParam {

    /**
     * 保留字段
     */
    private String result;
    /**
     * 保留字段
     */
    private String desc;

    /**
     * 数据集
     */
    private List<SmsDeliverData> delivers;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public List<SmsDeliverData> getDelivers() {
        return delivers;
    }

    public void setDelivers(List<SmsDeliverData> delivers) {
        this.delivers = delivers;
    }

    @Override
    public String toString() {
        return "SmsDeliverResponse{" +
                "result='" + result + '\'' +
                ", desc='" + desc + '\'' +
                ", delivers=" + delivers +
                '}';
    }
}
