package com.dahantc.sms.entity.param;

/**
 * 类描述：短信下发状态报告参数
 *
 * @author 8526
 * @date 2022-05-10 13:13:12
 * 版权所有 Copyright www.dahantc.com
 */
public class SmsReportData {

    /**
     * 短信编号
     */
    private String msgid;

    /**
     * 下行手机号码
     */
    private String phone;

    /**
     * 短信发送结果：0——成功；1——接口处理失败；2——运营商网关失败
     */
    private String status;

    /**
     * 状态报告描述
     */
    private String desc;

    /**
     * 当status为1时, 表示平台返回错误码，参考:状态报告错误码。当status为0或2时，表示运营商网关返回的原始值
     */
    private String wgcde;

    /**
     * 状态报告接收时间格式为yyyy-MM-dd HH:mm:ss
     */
    private String time;

    /**
     *  客户提交时间格式为yyyy-MM-dd HH:mm:ss
     */
    private String submitTime;

    /**
     *  大汉三通发送时间格式为yyyy-MM-dd HH:mm:ss
     */
    private String sendTime;

    /**
     * 长短信条数（接口处理失败只给一条）
     */
    private Integer smsCount;

    /**
     * 长短信序号
     */
    private Integer smsIndex;

    public String getMsgid() {
        return msgid;
    }

    public void setMsgid(String msgid) {
        this.msgid = msgid;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getWgcde() {
        return wgcde;
    }

    public void setWgcde(String wgcde) {
        this.wgcde = wgcde;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getSubmitTime() {
        return submitTime;
    }

    public void setSubmitTime(String submitTime) {
        this.submitTime = submitTime;
    }

    public String getSendTime() {
        return sendTime;
    }

    public void setSendTime(String sendTime) {
        this.sendTime = sendTime;
    }

    public Integer getSmsCount() {
        return smsCount;
    }

    public void setSmsCount(Integer smsCount) {
        this.smsCount = smsCount;
    }

    public Integer getSmsIndex() {
        return smsIndex;
    }

    public void setSmsIndex(Integer smsIndex) {
        this.smsIndex = smsIndex;
    }

    @Override
    public String toString() {
        return "SmsReportData{" +
                "msgid='" + msgid + '\'' +
                ", phone='" + phone + '\'' +
                ", status='" + status + '\'' +
                ", desc='" + desc + '\'' +
                ", wgcde='" + wgcde + '\'' +
                ", time='" + time + '\'' +
                ", submitTime='" + submitTime + '\'' +
                ", sendTime='" + sendTime + '\'' +
                ", smsCount=" + smsCount +
                ", smsIndex=" + smsIndex +
                '}';
    }
}
