package com.dahantc.sms.entity.param;

/**
 * 类描述：短信上行消息报告参数
 *
 * @author 8526
 * @date 2022-05-10 13:07:45
 * 版权所有 Copyright www.dahantc.com
 */
public class SmsDeliverData {

    /**
     * 回复手机号码
     */
    private String phone;

    /**
     * 手机回复短信内容
     */
    private String content;

    /**
     * 子号码
     */
    private String subcode;

    /**
     * 回复时间，格式yyyy-MM-dd HH:mm:ss
     */
    private String delivertime;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSubcode() {
        return subcode;
    }

    public void setSubcode(String subcode) {
        this.subcode = subcode;
    }

    public String getDelivertime() {
        return delivertime;
    }

    public void setDelivertime(String delivertime) {
        this.delivertime = delivertime;
    }

    @Override
    public String toString() {
        return "SmsDeliverData{" +
                "phone='" + phone + '\'' +
                ", content='" + content + '\'' +
                ", subcode='" + subcode + '\'' +
                ", delivertime='" + delivertime + '\'' +
                '}';
    }
}
