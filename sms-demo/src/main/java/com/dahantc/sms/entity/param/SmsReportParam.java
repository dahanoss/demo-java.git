package com.dahantc.sms.entity.param;

import java.util.List;

/**
 * 类描述：短信下发状态报告接收-参数
 *
 * @author 8526
 * @date 2022-05-10 13:06:36
 * 版权所有 Copyright www.dahantc.com
 */
public class SmsReportParam {

    /**
     * 保留字段
     */
    private String result;
    /**
     * 保留字段
     */
    private String desc;

    /**
     * 数据集
     */
    private List<SmsReportData> reports;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public List<SmsReportData> getReports() {
        return reports;
    }

    public void setReports(List<SmsReportData> reports) {
        this.reports = reports;
    }

    @Override
    public String toString() {
        return "SmsReportResponse{" +
                "result='" + result + '\'' +
                ", desc='" + desc + '\'' +
                ", reports=" + reports +
                '}';
    }
}
