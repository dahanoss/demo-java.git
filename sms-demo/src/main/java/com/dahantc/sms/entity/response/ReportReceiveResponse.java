package com.dahantc.sms.entity.response;

/**
 * 类描述：接收短信下发状态报告-返参
 * @author 8522
 * @date 2022-05-10 14:03:22
 * 版权所有 Copyright www.dahantc.com
 */
public class ReportReceiveResponse {
    //推送结果，成功时status的值返回success，其他值均认为推送失败。
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
