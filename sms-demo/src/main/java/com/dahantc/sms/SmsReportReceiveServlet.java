package com.dahantc.sms;

import com.alibaba.fastjson.JSON;
import com.dahantc.sms.entity.param.SmsReportParam;
import com.dahantc.sms.entity.response.ReportReceiveResponse;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * 类描述：接收短信下发状态报告
 *
 * @author 8526
 * @date 2022-05-10 13:23:28
 * 版权所有 Copyright www.dahantc.com
 */
public class SmsReportReceiveServlet extends HttpServlet {
    private static final long serialVersionUID = 252700719588234609L;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        ReportReceiveResponse res = new ReportReceiveResponse();
        res.setStatus("fail");
        try {
            String report = request.getParameter("report");
            SmsReportParam detailResponse = JSON.parseObject(report, SmsReportParam.class);
            System.out.println("收到响应结果：" + detailResponse);
            res.setStatus("success");
        } catch (Exception e) {
            System.out.println("推送解析异常：" + e.getMessage());
        }
        PrintWriter writer = response.getWriter();
        writer.print(JSON.toJSONString(res));
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        try (PrintWriter out = response.getWriter()) {
            out.print("Please use  POST method to submit data");
        } catch (Exception e) {
        }
    }

}
