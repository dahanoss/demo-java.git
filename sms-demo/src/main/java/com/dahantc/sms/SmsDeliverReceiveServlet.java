package com.dahantc.sms;

import com.alibaba.fastjson.JSON;
import com.dahantc.sms.entity.param.SmsDeliverParam;
import com.dahantc.sms.entity.response.DeliverReceiveResponse;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * 类描述：接收上行回复短信
 *
 * @author 8526
 * @date 2022-05-10 13:23:28
 * 版权所有 Copyright www.dahantc.com
 */
public class SmsDeliverReceiveServlet extends HttpServlet {
    private static final long serialVersionUID = 252700719588234609L;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        DeliverReceiveResponse res = new DeliverReceiveResponse();
        res.setStatus("failure");
        try {
            String deliver = request.getParameter("deliver");
            SmsDeliverParam deliverResponse = JSON.parseObject(deliver, SmsDeliverParam.class);
            System.out.println("收到响应结果：" + deliverResponse);
            res.setStatus("success");
        } catch (Exception e) {
            System.out.println("推送解析异常：" + e.getMessage());
        }
        PrintWriter writer = response.getWriter();
        writer.print(JSON.toJSONString(res));
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        try (PrintWriter out = response.getWriter()) {
            out.print("Please use  POST method to submit data");
        } catch (Exception e) {
        }
    }

}
