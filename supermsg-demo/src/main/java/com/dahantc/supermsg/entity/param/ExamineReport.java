package com.dahantc.supermsg.entity.param;

/**
 * 类描述：审核报告参数
 * @author 8522
 * @date 2022-05-10 14:00:43
 * 版权所有 Copyright www.dahantc.com
 */
public class ExamineReport {
    //上传时素材的模板ID
    private String mmsProductId;
    //该批短信提交结果；说明请参照：超级短信提交响应错误码
    private String code;
    //响应消息说明
    private String msg;
    //审核通过或未通过原因、备注
    private String auditMemo;

    public String getMmsProductId() {
        return mmsProductId;
    }

    public void setMmsProductId(String mmsProductId) {
        this.mmsProductId = mmsProductId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getAuditMemo() {
        return auditMemo;
    }

    public void setAuditMemo(String auditMemo) {
        this.auditMemo = auditMemo;
    }
}
