package com.dahantc.supermsg.entity.param;

import java.util.List;

/**
 * 类描述：超级短信发送状态报告接收-参数
 * @author 8522
 * @date 2022-05-10 14:17:04
 * 版权所有 Copyright www.dahantc.com
 */
public class SendReportReceiveParam {
    //结果代码
    private String result;
    //结果描述
    private String msg;
    //状态报告集合
    private List<SendReport> reports;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<SendReport> getReports() {
        return reports;
    }

    public void setReports(List<SendReport> reports) {
        this.reports = reports;
    }
}
