package com.dahantc.supermsg.entity.response;

/**
 * 类描述：审核报告接收-返参
 * @author 8522
 * @date 2022-05-10 14:03:22
 * 版权所有 Copyright www.dahantc.com
 */
public class ExamineReportReceiveResponse {
    //推送结果，成功时status的值返回success，其他值均认为推送失败。
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
