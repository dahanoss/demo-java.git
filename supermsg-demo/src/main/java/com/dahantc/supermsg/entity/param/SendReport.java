package com.dahantc.supermsg.entity.param;

/**
 * 类描述：超级短信发送状态报告参数
 * @author 8522
 * @date 2022-05-10 14:15:22
 * 版权所有 Copyright www.dahantc.com
 */
public class SendReport {
    //批次id
    private String msgId;
    //手机号码,对应请求包中的一个手机号码
    private String mobile;
    //彩信发送结果：0—成功（如果配置了需要下载状态，status为0时errorCode为RECEIVD表示下载成功）；1—接口处理失败； 2—运营商网关失败
    private String status;
    //当status为1时，可参考：处理失败错误码；当status为2时，表示运营商网关返回的原始值
    private String errorCode;
    //错误码描述
    private String statusDesp;
    //状态报告时间
    private String reportTime;

    public String getMsgId() {
        return msgId;
    }

    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getStatusDesp() {
        return statusDesp;
    }

    public void setStatusDesp(String statusDesp) {
        this.statusDesp = statusDesp;
    }

    public String getReportTime() {
        return reportTime;
    }

    public void setReportTime(String reportTime) {
        this.reportTime = reportTime;
    }
}
