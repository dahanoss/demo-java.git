package com.dahantc.supermsg.entity.param;

import java.util.List;

/**
 * 类描述：审核报告接收-参数
 * @author 8522
 * @date 2022-05-10 14:02:31
 * 版权所有 Copyright www.dahantc.com
 */
public class ExamineReportReceiveParam {
    //结果代码
    private String result;
    //结果描述
    private String desc;
    //报告集合
    private List<ExamineReport> reports;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public List<ExamineReport> getReports() {
        return reports;
    }

    public void setReports(List<ExamineReport> reports) {
        this.reports = reports;
    }
}
