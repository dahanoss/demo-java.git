package com.dahantc.supermsg;

import com.alibaba.fastjson.JSONObject;
import com.dahantc.ctc.utils.FileUtil;
import com.dahantc.supermsg.api.SupermsgClient;
import com.dahantc.supermsg.entity.param.ExamineReportParam;
import com.dahantc.supermsg.entity.param.QrySendReportParam;
import com.dahantc.supermsg.entity.param.SendSupermsgParam;
import com.dahantc.supermsg.entity.param.SupermsgTemp;
import com.dahantc.supermsg.entity.param.UpSupermsgTempParam;
import com.dahantc.supermsg.entity.response.ExamineReportResponse;
import com.dahantc.supermsg.entity.response.QrySendReportResponse;
import com.dahantc.supermsg.entity.response.SendSupermsgResponse;
import com.dahantc.supermsg.entity.response.UpSupermsgTempResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * 类描述：超级短信模块SDK调用示例
 * @author 8522
 * @date 2022-05-10 10:18:31
 * 版权所有 Copyright www.dahantc.com
 */
public class SupermsgDemo {
    private final static String ACCOUNT = "dhst8526";
    private final static String PASSWORD = "bb43a2c4081bec02fca7b72f38e63021";
    private final static String URL = "http://180.168.192.126:13766";
    private final static SupermsgClient supermsgClient = new SupermsgClient(URL);

    /**
     * 方法描述：上传超级短信模板
     *
     * @throws
     * @date 2022-05-09 17:00:14
     */
    public static void upTemp() {
        UpSupermsgTempParam upSupermsgTempParam = new UpSupermsgTempParam();
        upSupermsgTempParam.setAccount(ACCOUNT);
        upSupermsgTempParam.setPassword(PASSWORD);
        upSupermsgTempParam.setTitle("标题");
        List<SupermsgTemp> supermsgTemps = new ArrayList<>();
        SupermsgTemp supermsgTemp = new SupermsgTemp();
        supermsgTemp.setName("t1.png");
        supermsgTemp.setIndex("1_1");
        supermsgTemp.setContent(FileUtil.getBase64Encode("C:\\Users\\youju\\Desktop\\测试\\t1.png"));
        supermsgTemps.add(supermsgTemp);
        SupermsgTemp supermsgTemp2 = new SupermsgTemp();
        supermsgTemp2.setIndex("1_2");
        supermsgTemp2.setContent("${2,20}是一座美丽的城市，我爱${2,20}！");
        supermsgTemps.add(supermsgTemp2);
        upSupermsgTempParam.setContent(supermsgTemps);

        System.out.println("入参：" + JSONObject.toJSONString(upSupermsgTempParam));
        UpSupermsgTempResponse upSupermsgTempResponse = supermsgClient.upTemp(upSupermsgTempParam);
        System.out.println("返参：" + JSONObject.toJSONString(upSupermsgTempResponse));
    }

    /**
     * 方法描述：超级短信审核状态查询
     *
     * @throws
     * @date 2022-05-09 17:00:19
     */
    public static void qryExamine(){
        ExamineReportParam examineReportParam = new ExamineReportParam();
        examineReportParam.setAccount(ACCOUNT);
        examineReportParam.setPassword(PASSWORD);
        examineReportParam.setTemplateNo("ff808081804659220180a8115668000d");
        System.out.println("入参：" + JSONObject.toJSONString(examineReportParam));
        ExamineReportResponse examineReportResponse = supermsgClient.qryExamine(examineReportParam);
        System.out.println("返参：" + JSONObject.toJSONString(examineReportResponse));
    }

    /**
     * 方法描述：超级短信下发
     *
     * @throws
     * @date 2022-05-09 17:06:55
     */
    public static void sendSupermsg(){
        SendSupermsgParam sendSupermsgParam = new SendSupermsgParam();
        sendSupermsgParam.setAccount(ACCOUNT);
        sendSupermsgParam.setPassword(PASSWORD);
        sendSupermsgParam.setTemplateNo("ff808081804659220180a8115668000d");
        sendSupermsgParam.setMobiles("15236364545,15789525263");
        System.out.println("入参：" + JSONObject.toJSONString(sendSupermsgParam));
        SendSupermsgResponse sendSupermsgResponse = supermsgClient.sendSupermsg(sendSupermsgParam);
        System.out.println("返参：" + JSONObject.toJSONString(sendSupermsgResponse));
    }

    /**
     * 方法描述：获取超级短信状态报告
     *
     * @throws
     * @date 2022-05-09 17:09:56
     */
    public static void qrySendReport(){
        QrySendReportParam qrySendReportParam = new QrySendReportParam();
        qrySendReportParam.setAccount(ACCOUNT);
        qrySendReportParam.setPassword(PASSWORD);
        System.out.println("入参：" + JSONObject.toJSONString(qrySendReportParam));
        QrySendReportResponse qrySendReportResponse = supermsgClient.qrySendReport(qrySendReportParam);
        System.out.println("返参：" + JSONObject.toJSONString(qrySendReportResponse));
    }

    public static void main(String[] args) {
        //上传超级短信模板
        upTemp();

        //超级短信审核状态查询
        qryExamine();

        //超级短信下发
        sendSupermsg();

        //获取超级短信状态报告
        qrySendReport();
    }
}
