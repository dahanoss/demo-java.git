package com.dahantc.supermsg;

import com.dahantc.supermsg.servlet.ExamineReportServlet;
import com.dahantc.supermsg.servlet.SendReportServlet;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

/**
 * 类描述：超级短信报告接收示例
 * @author 8522
 * @date 2022-05-10 13:59:29
 * 版权所有 Copyright www.dahantc.com
 */
public class SupermsgReceiveMain {

    public static void main(String[] args) throws Exception {
        //审核状态报告接收服务
        Server server = new Server(8080);
        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setContextPath("/");
        server.setHandler(context);
        //http://localhost:8080/receiveExamineReport
        //审核状态报告接收服务
        context.addServlet(new ServletHolder(new ExamineReportServlet()), "/receiveExamineReport");
        //http://localhost:8080/receiveSendReport
        //发送状态报告接收服务
        context.addServlet(new ServletHolder(new SendReportServlet()), "/receiveSendReport");
        server.start();
        server.join();
    }
}
