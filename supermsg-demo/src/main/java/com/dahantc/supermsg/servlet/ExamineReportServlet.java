package com.dahantc.supermsg.servlet;

import com.alibaba.fastjson.JSONObject;
import com.dahantc.supermsg.entity.param.ExamineReportReceiveParam;
import com.dahantc.supermsg.entity.response.ExamineReportReceiveResponse;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 类描述：审核报告接收服务
 * @author 8522
 * @date 2022-05-10 14:07:09
 * 版权所有 Copyright www.dahantc.com
 */
public class ExamineReportServlet extends HttpServlet {
    private static final long serialVersionUID = 252700719588234609L;
    //key为ip，value为报告推送数
    private static Map<String, AtomicInteger> countMap = new HashMap<String, AtomicInteger>();
    //报告推送总数
    private static int num = 0;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        ExamineReportReceiveResponse examineReportReceiveResponse = new ExamineReportReceiveResponse();
        JSONObject message = JSONObject.parseObject(request.getParameter("supsmstempreport"));;
        PrintWriter out = null;
        String clientIp = getRemoteHost(request);
        examineReportReceiveResponse.setStatus("fail");
        try {
            if (message == null){
                message = getRequestData(request);
            }
            System.out.println("收到消息：" + message);
            if (null != message) {
                ExamineReportReceiveParam examineReportReceiveParam = JSONObject.toJavaObject(message, ExamineReportReceiveParam.class);
                if (null != examineReportReceiveParam) {
                    if (!countMap.containsKey(clientIp)) {
                        countMap.put(clientIp, new AtomicInteger());
                    }
                    System.out.println(clientIp + ":" + countMap.get(clientIp).addAndGet(examineReportReceiveParam.getReports().size()));
                    num += examineReportReceiveParam.getReports().size();
                }
                examineReportReceiveResponse.setStatus("success");
            }
            response.setCharacterEncoding("UTF-8");
            out = response.getWriter();
            out.print(JSONObject.toJSONString(examineReportReceiveResponse));
            System.out.println("收到状态报告总条数：" + num);
        } catch (Exception e) {
            System.out.println("接收报告失败：" + e);
        } finally {
            if (null != out) {
                out.close();
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        try (PrintWriter out = response.getWriter()) {
            out.print("Please use  POST method to submit data");
        } catch (Exception e) {
            System.out.println("入参转化为JSONObject对象失败：" + e);
        }
    }

    /**
     * 方法描述：获取入参转化为JSONObject对象
     * @param request
     * @return {@link JSONObject}
     * @date 2022-04-29 16:00:04
     */
    protected JSONObject getRequestData(HttpServletRequest request) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        try {
            InputStream inputStream = request.getInputStream();
            byte[] by = new byte[1024];
            int len;
            if (inputStream != null) {
                while ((len = inputStream.read(by)) != -1) {
                    outputStream.write(by, 0, len);
                }
                outputStream.flush();
            }
            return JSONObject.parseObject(new String(outputStream.toByteArray(), StandardCharsets.UTF_8));
        } catch (IOException e) {
            System.out.println("入参转化为JSONObject对象失败：" + e);
            try {
                outputStream.close();
            } catch (Exception e1) {
                System.out.println("入参转化为JSONObject对象失败：" + e1);
            }
        }
        return null;
    }

    public static String getRemoteHost(HttpServletRequest request) {
        String _ip = request.getHeader("X-Real-IP");
        if (_ip == null || _ip.length() == 0 || "unknown".equalsIgnoreCase(_ip)) {
            _ip = request.getHeader("Proxy-Client-IP");
        }
        if (_ip == null || _ip.length() == 0 || "unknown".equalsIgnoreCase(_ip)) {
            _ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (_ip == null || _ip.length() == 0 || "unknown".equalsIgnoreCase(_ip)) {
            _ip = request.getHeader("x-forwarded-for");
        }
        if (_ip == null || _ip.length() == 0 || "unknown".equalsIgnoreCase(_ip)) {
            _ip = request.getRemoteAddr();
        }
        return _ip;
    }
}
